module.exports = {
  'Test DSEO Promotional Offer' : function (browser) {
	var homepage = browser.page.HomePage()
    var orderpage = browser.page.OrderPage()	
    browser
	  .url('http://www.keepcollective.com')
	  .pause(10000)
      .assert.title('Personalized Keepsake Jewelry that Truly Matters | KEEP Collective');
	  homepage.waitForElementPresent('@shopLink', 10000);  
	  homepage.click('@shopLink');
	  homepage.waitForElementPresent('@whatsNewLink', 10000); 
	  homepage.click('@whatsNewLink');
	  homepage.waitForElementPresent('@collectionItem', 10000); 
	  browser.useXpath()
	  homepage.click('@selectedItem');
	  browser.useCss();
	  homepage.waitForElementPresent('@viewCartButton', 10000); 
	  homepage.click('@viewCartButton')
	  browser.pause(10000)
	  homepage.expect.element('@quantity').text.to.equal('1');
	  homepage.waitForElementPresent('@bag', 10000); 
	  homepage.click('@bag')
	  orderpage.waitForElementPresent('@editLink', 10000); 	
	  orderpage.click('@editLink')
	  orderpage.waitForElementPresent('@updateQty', 10000); 	  
	  orderpage.clearValue('@updateQty')
	  orderpage.setValue('@updateQty', '3')
	  orderpage.waitForElementPresent('@updateButton', 10000); 
	  orderpage.click('@updateButton')    
	  browser.pause(10000)
	  orderpage.assert.elementNotPresent('@promotionBanner')
	  orderpage.expect.element('@body').text.to.not.contain('You qualify for 50% off Exclusive Items!')
	  browser.useXpath()
	  homepage.click('@findHostessLink')
	  browser.useCss()
	  homepage.waitForElementPresent('@searchHostessTxtBox', 10000) 	
	  browser.pause(2000)
	  homepage.setValue('@searchHostessTxtBox','Allison Ahearn')
	  homepage.waitForElementPresent('@resultList', 10000);
	  homepage.click('@resultList')
	  homepage.waitForElementPresent('@startShoppingButton', 10000);
	  homepage.click('@startShoppingButton')
	  browser.pause(10000)
	  orderpage.assert.containsText('@promotionBanner',
		'You qualify for 50% off Exclusive Items!')
	  orderpage.click('@editLink')
	  orderpage.waitForElementPresent('@updateQty', 10000); 	  
	  orderpage.clearValue('@updateQty')
	  orderpage.setValue('@updateQty', '1')
	  orderpage.waitForElementPresent('@updateButton', 10000); 
	  orderpage.click('@updateButton')    
	  browser.pause(10000)
	  orderpage.assert.elementNotPresent('@promotionBanner');
	  orderpage.expect.element('@body').text.to.not.contain('You qualify for 50% off Exclusive Items!')
	  browser.end();
  }
};