#Prerequisite

Nodejs and nightwatch installed

#Steps to run the test

* Checkout the project folder from github 
* Go to the project folder and open command window there
* Type "node nightwatch.js"

#Scenarios

* Added items in the cart having worth more than $50, but no designer hence promotional offer is not displayed
* Added designer to the above scenario so the promotional offer is displayed 
* Removed items from cart making amount less than $50 so the promotional offer is no more valid

#Remarks

Already included required libraries like selenium server and geckodriver