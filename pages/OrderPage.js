'use strict';

module.exports = {  
   elements: {
    editLink: {
      selector: '.js-edit.edit-remove-link'
    },
	
	 updateQty: {
      selector: '.form-control.bh-only-number.bh-select-on-focus.qty'
    },
	
	 updateButton: {
      selector: '.btn.btn-sm.btn-primary.js-update'
    },
	
	 promotionBanner: {
      selector: ".text-center.promotions-banner"
    },
	
	 viewCartButton: {
      selector: '#view_cart'
    },
	
	quantity: {
      selector: '.qty'
    },
	
	bag: {
      selector: '#nav-bag'
    },
	
	body: {
      selector: '#content'
    }
  }
};