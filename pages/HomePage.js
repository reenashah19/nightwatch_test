'use strict';

module.exports = {  
   elements: {
    shopLink: {
      selector: '#nav_shop'
    },
	
	 whatsNewLink: {
      selector: '#whats_new'
    },
	
	 collectionItem: {
      selector: '.price'
    },
	
	 selectedItem: {
		 locateStrategy: 'xpath',
      selector: ".//div[contains(.,'$25.00') and @class='price']"
    },
	
	 viewCartButton: {
      selector: '#view_cart'
    },
	
	quantity: {
      selector: '.qty'
    },
	
	bag: {
      selector: '#nav-bag'
    },
	
	findHostessLink: {
		locateStrategy: 'xpath',
      selector: "//div[@class='nav-top']//a[@class='find-hostess']"
    },
	
	searchHostessTxtBox: {
      selector: '.form-control.js-search-field'
    },
	
	resultList: {
      selector: '.js-result-item.result-item'
    },
	
	startShoppingButton: {
      selector: '#start-shopping'
    }
  }
};